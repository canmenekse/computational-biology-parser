#C:\strawberry\bin\perl.exe
$|=1;

use warnings;
use strict;
##########################Gets the translation when there is one################################
#Translations are stored in the file like:/translation="MVLVLFFIVTCFGIECPNNTFFDNGNCVVCSP
#YCVEGKCDIEKGCVECINHFQESNKLCESSCEENKYYDGDNCVDCSQHCINGYCDKLDGCTLCQRGYLAFNKVVVI"
#And so it basically filter around the quotes
################################################################################################
sub getTranslation
{
    my $translated;
    my @char_Arr;
    my $entry=$_[0];
    my $cdingGene=$_[1];
    if($cdingGene==1)
    {
      #Gets the translation part
      if($entry=~m/\/translation\=\"(.*?)\"/s)
      {
       @char_Arr = split //, $1;
        for(my $i=0;$i<scalar(@char_Arr);$i=$i+1)
        {
            #There are white spaces in front in the file
            if($char_Arr[$i]=~m/[^\s]/)
            {
            $translated=$translated.$char_Arr[$i];
            }
        }
        return $translated;
        
      }
    }
   else
   {
            return "NA";
   }
}
##########################Gets the protein Id when there is one################################
#Basically gets the protein Id from a line like: /protein_id="NP_395225.1"
#it does that with a simple regex filtering the quotes
###############################################################################################
sub getProteinId
{
   my $entry=$_[0];
   my $cdingGene=$_[1];
   if($cdingGene==1)
   {
     if($entry=~m/\/protein_id\=\"(.*)\"/)
     {
         return $1;
     }
   }
   else
   {
        return "NA";
   }
}
##########################Gets the product when possible########################################
#Basically gets the product from a line like: /product="putative transposase"
#it does that with a simple regex filtering the quotes
################################################################################################
sub getProduct
{
    my $entry=$_[0];
    my $cdingGene=$_[1];
    if($cdingGene==1)
    {
      if($entry=~m/\/product\=\"(.*?)\"/s)
      {
        return $1;
      }
    }
   else
   {
            return "NA";
   }
}
##########################Check whether it is cdingGene#########################################
#Checks whether that it is one of the genes that contain CDS ,we do that because non CDS
#genes do not need to be checked with some information like Translation, Product and Protein ID.
################################################################################################
sub isCdingGene
{
    my $entry=$_[0];
    if($entry=~m /CDS\s+/)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
##########################Returns 1 when it is complement########################################
#when in the information there exists a line containing: complement(68947..69378)
#information like that, it checks whether it is a complement or not and returns 1 when it is
#and returns null when it is not
################################################################################################
sub isComplement
{
    my $entry=$_[0];
    if($entry=~m/gene\s+(?:complement)(?:\()(\d+)\.+\d+/)
    {
        return 1;
    }
    return 0;
}
##########################Returns the start interval in the string##############################
#So as we know there are intervals of genes in the file and this gets the start interval of the
#gene as in the example of : YPCD 1.96c it has a line containing:complement(68947..69378)
#so it returns the value 68947 in that example
################################################################################################
sub getStartInterval

{
   my $entry=$_[0];
   if($entry=~m/gene\s+(?:complement)?(?:\()?(\d+)\.+\d+/)
    {
        return $1;
    
    }
    return "N\\A";
}
##########################Returns the Last Interval of the string###############################
#So as we know there are intervals of genes in the file and this gets the last interval of the
#gene as in the example of : YPCD 1.96c it has a line containing:complement(68947..69378)
#so it returns the value 69378 in that example
################################################################################################
sub getLastInterval
{
    my $entry=$_[0];
   if($entry=~m/gene\s+(?:complement)?(?:\()?\d+\.+(\d+)/)
    {
        return $1;
    
    }
    return "N\\A";
}
##########################Gets the locus tag of the gene########################################
#Returns the locus tag by filtering around the quotes.
#an example looks like: /locus_tag="YPCD1.96c"
################################################################################################
sub getLocusTag
{
    my $entry=$_[0];
    if($entry=~ m /\/locus_tag=\"(.*)"/)
    {
       return $1;
    }
    else
    {
    return "N/A";
    }
}
##########################Get from the string and parse only the genes##########################
#What it does is that it basically gets all the genes and seperate them into an array
#The regex that does is: 
#1)(gene.*?)
#Check whether there is word "gene" but do it Ungreedy (When it is greedy it matches the first
#occurence of the gene in the file and the last occurence of the gene in that file that is nearly
#the whole file
#2)(?!gene[\=\_r])) (?!gene\s*in)
#then we do not want any words between two "genes" because there exist some words like /gene
#gene_ and genes in so to avoid that we put these.
#p.s:(?=gene)
#Also we do want to search the next gene in our next search so we do not want the word gene
#to be consumed
#And the last part is to not forget the last one before the ORIGIN
################################################################################################
sub parseGnes
{
    my $arr=shift(@_);
    my $contents=shift(@_);
    my $check=0;
    while($contents=~ m /(gene.*?)((?!gene[\=\_r])) (?!gene\s*in)(?=gene)/gs)
    {
        if($check==1)
        {
            push(@$arr,$1);
        }
        else
        {
            $check=1;
        }
    }
    #To add the last codon
    if($contents=~m/.*(gene.*?)((?!gene[\=\_r])) (?!gene\s*in)(?=ORIGIN)/s)
    {
        push(@$arr,$1);
    }
}
##########################Print the contents of an array########################################
#It prints the contents of an array by seperating them with a newline
################################################################################################
sub printArray
{
    my $printArrayRef=shift(@_);
    my @arr=@$printArrayRef;
    my $file=shift(@_);
   for(my $i=0;$i<scalar(@arr);$i=$i+1)
   {
        print $file $arr[$i] ."\n";
   }
}
##########################Prints the part of properties#########################################
#It is used in the method Print Genes to print whether it is complement or not
################################################################################################
sub printGeneIntervals
{
    my $startInterval=$_[0];
    my $lastInterval=$_[1];
    my $complement=$_[2];
    my $file=$_[3];
    if($complement==1)
    {
        print $file "Complement" . "(". $startInterval. " .. " . $lastInterval.")\n";
    }
    elsif($complement==0)
    {
        print $file "(". $startInterval . " .. ". $lastInterval . ")\n";
    }

}
#########################Gets the ATCG sequences from file and then puts them into hsh#########
#It gets the information about the ATCG sequences(Amino acid sequences) and how it does is the
#following: 
#an example: 68281 tacgctcgcc agttgcgtcc cactggtgtc attaatgttt aactgactcc acaatagcca
#it puts 68281 in the sequencesInterval Array and it puts 68281 as the key and the sequence
#as the value 
#Also It does get all the AminoAcids and put them in charrArray
###############################################################################################
sub getSequence
{
   
    my $content=$_[0];
    my $sequencesRef=$_[1];
    my $sequenceIntervalsRef=$_[2];
    my $charArrayRef=$_[3];
    my $prev=1;
    my $curent=1;
    my $interval=0;
    my $sequences=$content=~m/(ORIGIN.*)/s;
    $sequences=$1;
  #  print $sequences;
    while($sequences=~m/\s+(?#matches the spce)(\d+)\s+(.*)/g)
    {
       
        $curent=$1;
        $$sequencesRef{$1}=$2;
        $interval=$curent-$prev;
        $prev=$curent;
        push(@$sequenceIntervalsRef,$1);
    }
    
    #
    
    #Newly added
    my @arr_ = split //, $sequences;
   
     
   
    my $count=0;
    for(my $i=0;$i<scalar(@arr_);$i=$i+1)
    {
        
        if($arr_[$i]=~m/[acgt]/)
        {
            $count++;
           # print $arr_[$i];
            push(@$charArrayRef,$arr_[$i]);
        }
    }
    
    #print "count is " . $count . "\n";
    return $interval;
}

#########################It finds search intervals to get a.acid sequence######################
#Finding the start Interval:
#It checks all the values in the Sequence array(that is only composed of the values such as 
#69601,69601+interval)So when it founds an value that is Bigger than the search interval of the
#gene it stops and get the previous one hence an example of that could be :
#Let's say we search 69602 when we see the value 69601+interval we went too far in the array
#and we want to get some a.acids from the previous gene and thats why whe use the prevValue
#When the start Interval of the gene is same with the interval in the array then we can
#use that value.
#Finding the last Interval:
#When we search a last interval and found that the interval in the array is bigger or equal
#than the interval we search we use that value
#It also finds the index of that values in the sequence array
###############################################################################################
sub searchIntervalsInArray
{
 #initializations
 my $startInterval=shift(@_);
 my $lastInterval=shift(@_);
 my $arrReference=shift(@_);
 my $startArrayInterval=shift(@_);
 my $lastArrayInterval=shift(@_);
 my $indexOfStartArrayInterval=shift(@_);
 my $indexOfLastArrayInterval=shift(@_);
 my $quit=0;
 my $prevValue=0;
 my @arr=@$arrReference;
 #
 for(my $i=0;$i<scalar(@arr)&&$quit!=1;$i=$i+1)
    {
        
        if($startInterval<$arr[$i])
        {
           
            $$startArrayInterval=$prevValue;
            $$indexOfStartArrayInterval=$i-1;
            $quit=1;
        }
        elsif($startInterval==$arr[$i])
        {
       
        $$startArrayInterval=$arr[$i];
        $$indexOfStartArrayInterval=$i;
        $quit=1;
        }
        $prevValue=$arr[$i];
        
    }
   
##Last index
$quit=0;


for(my $i=0;$i<scalar(@arr)&&$quit!=1;$i=$i+1)
{
   if($lastInterval<=$arr[$i])
   {
        $$lastArrayInterval=$arr[$i];
        $$indexOfLastArrayInterval=$i;
        $quit=1;
   }
}        
 #print "\n"."You should search from " . $$lastArrayInterval ."\n";
}
#########################Prints the RNA of genes that are in the complement strand#############
#It is used only in debugging
###############################################################################################
sub mRna_temp
{
    my $sequence=shift(@_);
     print "before taking complement ". " it is ". $sequence . "\n";
    $sequence=takeComplement($sequence);
    print "after taking complement ". " it is ". $sequence . "\n";
    $sequence=~tr/Tt/Uu/;
     print "after chng  ". " it is ". $sequence . "\n";
    $sequence=reverse $sequence;
    print "after rvr ". " it is ". $sequence . "\n";
    print $sequence;
    
    


}
########################Print Protein Sequence#################################################
#Prints
###############################################################################################
sub printProteinSequence
{
    my $sequence=shift(@_);
    $sequence=~tr/acgtu/ACGTU/;
    my @char_arr = split //, $sequence;
    my $i2=0;
    my $file=shift(@_);
    for(my $i=0;$i<scalar(@char_arr);$i=$i+1)
    {
        $i2=$i2+1;
        print $file $char_arr[$i];
        if($i2==60)
        {
            print $file "\n";
            $i2=0;
        }
        
    }
}
########################Convert to mrna########################################################
#Convert all aminoacid sequences to Mrna and then put them in an array
###############################################################################################
sub toMrna
{
 my $mrnaArrayRef=shift(@_);
 my $proteinSequencesRef=shift(@_);
 my $isComplmntRef=shift(@_);
 my $translated;
 my @protein_Arr=@$proteinSequencesRef;
 my @complmnt_Arr=@$isComplmntRef;
 
 for(my $i=0;$i<scalar(@protein_Arr);$i=$i+1)
 {
    if($complmnt_Arr[$i]==0)
    {
        #print "\n the normal sequence is: \n".$protein_Arr[$i];
        
        $protein_Arr[$i]=takeComplement($protein_Arr[$i]);
        $protein_Arr[$i]=~tr/t/u/;
        $protein_Arr[$i]=reverse($protein_Arr[$i]);
      # print "\n chng sequence is : \n".$protein_Arr[$i];
   
   
    }
    elsif($complmnt_Arr[$i]==1)
    {
        $protein_Arr[$i]=~tr/t/u/;
    }
    
    push(@$mrnaArrayRef,$protein_Arr[$i]);
    #print $protein_Arr[$i];
 }




}
########################Split Codons###########################################################
#An example Aminoacid Sequence: AGCAGTAGTG it splits like AGC AGT AGT G
#Homewer it doesnt' do $codonHsh{G}
###############################################################################################
sub splitCodons
{

    my $codonHshRef=shift(@_);
    my $sequence=shift(@_);
    my $yeastProteins="";
    
    my %codonHsh=%$codonHshRef;
    
    
   # print $codonHsh{"GAAwaeaweaw"};
 #print $sequence;
 $sequence=~tr/agct/AGCT/;
    my $protein="";
    for(my $i=1;$i<=length($sequence);$i=$i+1)
    {
        
        $protein=$protein.substr($sequence,$i-1,1);
         
        if($i%3==0&&$i!=0)
        {
              # print "\n codonHsh{".$protein."}\n";
                if($codonHsh{$protein}ne "*")
                {
                $yeastProteins=$yeastProteins.$codonHsh{$protein};
                }
                $protein="";
        }
    
    }
    return $yeastProteins;
}
########################Creates the CodonHash to translate ####################################
#It basically puts the key value pairs into the hash to access them with their keys
###############################################################################################
sub setCodons
{
    my $codonHshRef=shift(@_);
    
    $$codonHshRef{"TTT"}="F";
    $$codonHshRef{"TTC"}="F";
    $$codonHshRef{"TTA"}="L";
    $$codonHshRef{"TTG"}="L";
    
    $$codonHshRef{"CTT"}="T";
    $$codonHshRef{"CTC"}="T";
    $$codonHshRef{"CTA"}="T";
    $$codonHshRef{"CTG"}="T";
    
    $$codonHshRef{"ATT"}="I";
    $$codonHshRef{"ATC"}="I";
    $$codonHshRef{"ATA"}="M";
    $$codonHshRef{"ATG"}="M";
    
    $$codonHshRef{"GTT"}="V";
    $$codonHshRef{"GTC"}="V";
    $$codonHshRef{"GTA"}="V";
    $$codonHshRef{"GTG"}="V";

    $$codonHshRef{"TCT"}="S";
    $$codonHshRef{"TCC"}="S";
    $$codonHshRef{"TCA"}="S";
    $$codonHshRef{"TCG"}="S";
    
    $$codonHshRef{"CCT"}="P";
    $$codonHshRef{"CCC"}="P";
    $$codonHshRef{"CCA"}="P";
    $$codonHshRef{"CCG"}="P";
    
    $$codonHshRef{"ACT"}="T";
    $$codonHshRef{"ACC"}="T";
    $$codonHshRef{"ACA"}="T";
    $$codonHshRef{"ACG"}="T";
    
    $$codonHshRef{"GCT"}="A";
    $$codonHshRef{"GCC"}="A";
    $$codonHshRef{"GCA"}="A";
    $$codonHshRef{"GCG"}="A";
    
    $$codonHshRef{"TAT"}="Y";
    $$codonHshRef{"TAC"}="Y";
    $$codonHshRef{"TAA"}="*";
    $$codonHshRef{"TAG"}="*";
    
    $$codonHshRef{"CAT"}="H";
    $$codonHshRef{"CAC"}="H";
    $$codonHshRef{"CAA"}="Q";
    $$codonHshRef{"CAG"}="Q";
    
    $$codonHshRef{"AAT"}="N";
    $$codonHshRef{"AAC"}="N";
    $$codonHshRef{"AAA"}="K";
    $$codonHshRef{"AAG"}="K";
    
    $$codonHshRef{"GAT"}="D";
    $$codonHshRef{"GAC"}="D";
    $$codonHshRef{"GAA"}="E";
    $$codonHshRef{"GAG"}="E";
    
    $$codonHshRef{"TGT"}="C";
    $$codonHshRef{"TGC"}="C";
    $$codonHshRef{"TGA"}="W";
    $$codonHshRef{"TGG"}="W";
    
    $$codonHshRef{"CGT"}="R";
    $$codonHshRef{"CGC"}=" ";
    $$codonHshRef{"CGA"}=" ";
    $$codonHshRef{"CGG"}="R";
    
    $$codonHshRef{"AGT"}="S";
    $$codonHshRef{"AGC"}="S";
    $$codonHshRef{"AGA"}="R";
    $$codonHshRef{"AGG"}="R";
    
    $$codonHshRef{"GGT"}="G";
    $$codonHshRef{"GGC"}="G";
    $$codonHshRef{"GGA"}="G";
    $$codonHshRef{"GGG"}="G";
    


}
########################Prints ################################################################
#Just prints
###############################################################################################
sub printSequenceWithoutNewline
{
    my $sequence=shift(@_);
    my $i2=0;
    my $file=shift(@_);
    
    for(my $i=0;$i<length($sequence);$i=$i+1)
    {
        $i2=$i2+1;
        print $file substr($sequence,$i,1);
        if($i2==60)
        {
            print $file "\n";
            $i2=0;
        }
        
    }

}



sub printNextPart
{
    my $locusTagsRef=shift(@_);
    my @locusTags=@$locusTagsRef;
    my $translationsRef=shift(@_);
    my $yeastCodonsRef=shift(@_);
    my $cdingGenesRef=shift(@_);
    my @translations=@$translationsRef;
    my @yeastCodons=@$yeastCodonsRef;
    my @cdingGenes=@$cdingGenesRef;
    my $file=shift(@_);
    for(my $i=0; $i<scalar(@locusTags);$i=$i+1)
    {
       if($yeastCodons[$i]ne $translations[$i]&&$cdingGenes[$i]!=0)
        {
           print $file "Locus Tag: " .$locusTags[$i] . "\ndoes not have the same proteins with yeast codons\n";
           printFasta($locusTags[$i],$yeastCodons[$i],$file);
           print $file "\n\nWHY THEY DON'T MATCH:\n";
           printDifferences($translations[$i],$yeastCodons[$i],$file);
           print $file "\n\n";
        }
    
    }
    
}


sub printPart_one_or_two
{
    my $locusTagsRef=shift(@_);
    my $proteinSequenceRef=shift(@_);
    my $file=shift(@_);
    my @locusTags=@$locusTagsRef;
    my @aminoAcidSequences=@$proteinSequenceRef;
    for(my $i=0;$i<scalar(@locusTags);$i=$i+1)
    {
        printFasta($locusTags[$i],$aminoAcidSequences[$i],$file);
        print $file "\n\n";
    }
    
}










sub printFasta
{
    my $locusTag=shift(@_);
    my $sequence=shift(@_);
    my $file=shift(@_);
    print $file ">".$locusTag."\n";
    printProteinSequence($sequence,$file);
   
}
########################Prints where the first difference occurs between Sequences#############
#Compares two Protein Sequences and finds the difference between them.
###############################################################################################
sub printDifferences
{
    my $s1=shift(@_);
    my $s2=shift(@_);
    my $file=shift(@_);
    if(length($s1)!=length($s2))
    {
        print $file "\nTheir lengths does not match\nlength of the translation is : " . length($s1). "\n";
        print $file "length of the yeast proteins are : " . length($s2). "\n";
    }
    else
    {
        print $file "\nTheir differences are : \n ";
        my @char_ArrTranslation=split //, $s1;
        my @char_ArrYeast=split //, $s2;
        my $printDifference=0;
        for(my $i=0;$i<scalar(@char_ArrTranslation)&&$printDifference==0;$i=$i+1)
        {
            
            if($char_ArrTranslation[$i]ne $char_ArrYeast[$i])
            {
                print $file "\nDifference at char " . ($i+1). " \nTranslation: '" .$char_ArrTranslation[$i]. "'\nYeast: '" .$char_ArrYeast[$i]. "' \n"; 
                $printDifference=1;
            }
        
        }
    }
}
########################Prints the gnes #######################################################
#Print the genes nicely.
###############################################################################################
sub printGenes
{
    ##Gettsng the parameters
    my $locusTagsArrayRef=shift(@_);
    my @locusTags=@$locusTagsArrayRef;
    my $startIntervalsArrayRef=shift(@_);
    my @startIntervals=@$startIntervalsArrayRef;
    my $lastIntervalsArrayRef=shift(@_);
    my @lastIntervals=@$lastIntervalsArrayRef;
    my $complementsArrayRef=shift(@_);
    my @complements=@$complementsArrayRef;
    my $cdingGenesArrayRef=shift(@_);
    my @cdingGenes=@$cdingGenesArrayRef;
    my $productsArrayRef=shift(@_);
    my @products=@$productsArrayRef;
    my $proteinIdsArrayRef=shift(@_);
    my @proteinIds=@$proteinIdsArrayRef;
    my $translationsArrayRef=shift(@_);
    my @translations=@$translationsArrayRef;
    my $ProteinSequenceOfGneArrayRef=shift(@_);
    my @proteinSequences=@$ProteinSequenceOfGneArrayRef;
    my $rnaProteinSequenceOfGneArrayRef=shift(@_);
    my @rnaProteinSequences=@$rnaProteinSequenceOfGneArrayRef;
    my $yeastCodonRef=shift(@_);
    my @yeastCodons=@$yeastCodonRef;
    my $differentLocusTagRef=shift(@_);
    my $file=shift(@_);
    
    
    ##check All the elmnt of array.
    for(my $i=0;$i<scalar(@locusTags);$i=$i+1)
    {
        print $file "////////////////////////GENE START///////////////////////////////////"."\n";
        print $file "GENE NUMBER :" . $i . "  \nLOCUS TAG : " .$locusTags[$i] ."\n";
        
        print $file "START_INTERVAL=".$startIntervals[$i]."\n";
        print $file "LAST_INTERVAL=".$lastIntervals[$i]."\n";
        print $file "IS_COMPLEMENT=" . $complements[$i]."\n";
        #printGeneIntervals($startIntervals[$i],$lastIntervals[$i],$complements[$i],$file); 
        print $file "CDS: " .$cdingGenes[$i]. " \nPROTEIN ID : " .$proteinIds[$i] . " \nPRODUCT: " .$products[$i] ."\n";
        print $file "\nTRANSLATION :\n";
        printSequenceWithoutNewline($translations[$i],$file); 
        print $file "\n";
        print $file "\nPROTEIN SEQUENCE :\n\n" ;
        print $file ">".$locusTags[$i]."\n";
        printProteinSequence($proteinSequences[$i],$file);
        print $file "\n\nMRNA SEQUENCE : \n\n";
        printProteinSequence($rnaProteinSequences[$i],$file);
        print $file "\n\nYEAST codon Table: \n";
        if($cdingGenes[$i]==1)
        {
        printSequenceWithoutNewline($yeastCodons[$i],$file);
        }
        if($yeastCodons[$i]ne $translations[$i]&&$cdingGenes[$i]!=0)
        {
           #print $file "\n THEY ARE DIFFERENT\n";
           #printDifferences($translations[$i],$yeastCodons[$i],$file);
           push(@$differentLocusTagRef,$locusTags[$i]);
        }
        print $file "\n////////////////////////GENE END///////////////////////////////////"."\n\n\n\n\n\n";
    }
}

########################Extracting Amino Acid Sequences #######################################
#Extract Amino Acid Sequences from Char Array
###############################################################################################

sub extractFromCharArray
{
    my $charArrayRef=shift(@_);
    my $start=shift(@_);
    my $lst=shift(@_);
    my @charArr=@$charArrayRef;
    my @proteinSequence;
    my $extractedSequence;
    
    for(my $i=$start-1;$i<$lst;$i=$i+1)
    {
        push(@proteinSequence,$charArr[$i]);
    }
    
   
    
    for(my $i=0;$i<scalar(@proteinSequence);$i=$i+1)
    {
        $extractedSequence=$extractedSequence.$proteinSequence[$i];
    }
    return $extractedSequence;
    
}

########################Prints the gnes #######################################################
#Takes the complement of a gene
###############################################################################################
sub takeComplement
{
    my $sequence=shift(@_);
    $sequence=~tr/ATGCatgcu/TACGtacga/;
    return $sequence;
}
################################################################################################

#Opens the file
open my $IN ,'<', 'input.txt' or die "Unable to open $ARGV[0]\n";
open (my $MYFILE, '>parsed.txt');

 

print $MYFILE "Welcome,So information of the genes are situated from the beginning until the word 'P A R T S:'"."\n";
print $MYFILE "However there is no whitespace between the characters of the word P A R T S:\n"; 
print $MYFILE "After that it is easy to see because i divided\n";
print $MYFILE "that part to make it easier to parse\n";


my @lines=<$IN>;
my $contents="@lines";
close $IN;
my $count=0;
my $interval;
my @gne;
#Array And hash initilizations
my @arr_locusTags;
my @arr_startIntervals;
my @arr_lastIntervals;
my @arr_complements;
my @arr_cdingGenes;
my @arr_products;
my @arr_proteinIds;
my @arr_translations;
my %hsh_sequences;
my @arr_sequenceIntervals;
my @arr_proteinSequenceOfGne;
my @charArray;
my @arr_mrna;
my %hsh_codons;
my @yeast_codons;
my @arr_differentLocusTags;


#Gets the genes from the file
parseGnes(\@gne,$contents);
foreach my $entry(@gne)
{
    ##Get the necessary things
    my $locusTag=getLocusTag($entry);
    my $startInterval=getStartInterval($entry);
    my $lastInterval=getLastInterval($entry);
    my $complement=isComplement($entry);
    my $cdingGene=isCdingGene($entry);
    my $product=getProduct($entry,$cdingGene);
    my $proteinId=getProteinId($entry,$cdingGene);
    my $translation=getTranslation($entry,$cdingGene);
    
    ##Push them into arrys
    push(@arr_locusTags,$locusTag);
    push(@arr_startIntervals,$startInterval);
    push(@arr_lastIntervals,$lastInterval);
    push(@arr_complements,$complement);
    push(@arr_cdingGenes,$cdingGene);
    push(@arr_products,$product);
    push(@arr_proteinIds,$proteinId);
    push(@arr_translations,$translation);
}
#Gets the Yeast Codons
setCodons(\%hsh_codons);

$interval=getSequence($contents,\%hsh_sequences,\@arr_sequenceIntervals,\@charArray);
my $codonSequence;
my $particularSeq;
#Again check all the genes and get more properties
for(my $i=0;$i<scalar(@arr_startIntervals);$i=$i+1)
{
$particularSeq= extractFromCharArray(\@charArray,$arr_startIntervals[$i] ,$arr_lastIntervals[$i]);
    if($arr_complements[$i]==1)
    {
        $particularSeq=~tr/agct/tcga/;
        $particularSeq=reverse $particularSeq;
    }
    push(@arr_proteinSequenceOfGne,$particularSeq);
    
    $codonSequence=splitCodons(\%hsh_codons,$particularSeq);
    push(@yeast_codons,$codonSequence);
    
}
#Converts to Mrna
toMrna(\@arr_mrna,\@arr_proteinSequenceOfGne,\@arr_complements);
#Print the Genes
print $MYFILE "\nGENERAL INFORMATION ABOUT A WHOLE GENE\n " ;
printGenes(\@arr_locusTags,\@arr_startIntervals,\@arr_lastIntervals,\@arr_complements,\@arr_cdingGenes,\@arr_products,\@arr_proteinIds,\@arr_translations,\@arr_proteinSequenceOfGne,\@arr_mrna,\@yeast_codons,\@arr_differentLocusTags,$MYFILE);
print $MYFILE "Different locus tags are : \n";
printArray(\@arr_differentLocusTags,$MYFILE);
my $count_=0;
for(my $i=0;$i<scalar(@arr_cdingGenes);$i=$i+1)
{
    if($arr_cdingGenes[$i]==1)
    {
    $count_=$count_+1;
    }
}
#print $MYFILE " it is " .$count_;
print $MYFILE "\n\nPARTS : \n\n\n\n";
print $MYFILE "\n\nPART ONE : \n\n\n\n";
printPart_one_or_two(\@arr_locusTags,\@arr_proteinSequenceOfGne,$MYFILE);
print $MYFILE "\n\nPART TWO : \n\n\n\n";
printPart_one_or_two(\@arr_locusTags,\@arr_mrna,$MYFILE);
print $MYFILE "\n\nPART THREE : \n\n\n\n";
printNextPart(\@arr_locusTags,\@arr_translations,\@yeast_codons,\@arr_cdingGenes,$MYFILE);
print "DONE! Check parsed.txt";

close ($MYFILE); 

































